# map function 

# map function takes 2 parameters 
# map (function , iterator for function ) 
# usage fill elements of sets,lists, touples,dicts...

#example 
def AddOneToResult(x):
    return x+1 

# create list that uses map function 
list_odd_num = list(map(AddOneToResult,range(0,10,2)))

print(list_odd_num)

# using lambda function 
list_cube = list(map(lambda x : x**3,range(10)))
print(list_cube)

# multi parameter map 
def AddAandB(a,b):
    return a+b

list_sum = list(map(AddAandB,range(0,10),range(10,20)))
print(list_sum)
# 0 + 10 , 1 + 11 , 2 + 12 ...


#using map to create list of lists from list

list_arc = ['AMD','Intel','ARM']

list_of_lists = list(map(list,list_arc))
print(list_of_lists)





