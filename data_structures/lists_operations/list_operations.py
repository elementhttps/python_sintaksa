# python list operations

#creating list 
list_int = [1,2,4,8,16,32,64,128,256]

# append(item) | add item to the end of the list  
list_int.append(512)
print(list_int)

# insert(position,item)| insert item at specific position insert(position,item)
list_int.insert(len(list_int),1024)
print(list_int)

# remove(item) | removes firs item that has this value 
list_int.remove(16)
print(list_int)


# pop(position) | removes item at position if empty removes last item 
list_int.pop(0)
print(list_int)

# empty list or clear list 
#NOTE .clear() requires python3.3
list1 = [1,2,3]
list2 = ['a','z']
list3 = [10,'zzz']

list1.clear()
del list2[:]
list3[:]=[]
print(list1,list2,list3)
 
# count(item) | how many times item appears in list 
print(list_int.count(1024))
list_int.append(1024)
print(list_int.count(1024))


# reverse() | reverses list [1,2] [2,1]
list_int.reverse()
print(list_int)


# index(item) or index(item,index) | find first index of item 

print(list_int.index(1024))
print(list_int.index(1024,1))
 
#NOTE: if list contains duplicate items index() will only return first 

# list comperhentions 
# creating a list in specific way 

cube = [x**3 for x in range(10)]
print(cube)


# 2 4 8 16 32 64 
#  x x2 x3 x4 x5 x6 

bits = [2**x for x in range(1,20)]
print(bits)

# map function
# map(function , iterator for function) 

list_map=list(map(lambda x : (2**x)+1,range(10)))
print(list_map)




