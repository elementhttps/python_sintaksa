
# function arguments 
def fun_arguments(stringA,intB=42,stringB = 'hello there'):
    print(stringA)
    print(intB+1)
    print(stringB)

#calling multi parameter function


fun_arguments('hi')
fun_arguments('hello',5)
fun_arguments('general kenoby',5,'*puls lightsaber')


#function test whether or not sequence has a certian value 

numSequence=10
def function_number_in_sequence(agr=numSequence):
    print(agr)

function_number_in_sequence()

# mutability
#shares default value 
def mutable(intA,listB=[]):
    listB.append(intA)
    return listB

print(mutable(5))
print(mutable(10))
#[5]
#[5,10]


# how not to share default variable 
def print_list(intC,listD=None):
    if listD is None:
        listD=[]
    listD.append(intC)
    return listD


print(print_list(11))
print(print_list(12))
# value is not shared output is
#11
#12


#keyword arguments
# one required argument and 3 optional arguments 
def fun_arguments(argA,argB='B argument',argC='c argument',argD='D argument'):
    print(argA)
    print(argB)
    print(argC)
    print(argD)


#calling function 

fun_arguments(42)
fun_arguments(argA=43)
fun_arguments(argA=44,argB='test')
fun_arguments(argB= 'test2',argA=45)
fun_arguments('this','is','sparta')
fun_arguments('hello there', argB = 'General Kenoby')

# what are bad calls ?
#fun_arguments() # required argument missing
#fun_arguments(argA = 10,'test') # non key aargument 
#fun_arguments(42,argA=43) # duplicate arguments 
#fun_arguments(missingArg='wut') #missing arg 



# Keywords and arguments 
# *argument , **keywords 

def buble_gum_crissis(ostName,*ostArgs,**ostKeywords):
    print('ost name is',ostName)
    for arg in ostArgs:
        print(arg)
    for kw in ostKeywords:
        print(kw,':',ostKeywords[kw])

# calling function 

buble_gum_crissis('Bublegum crisis',
                  'KnightSabers',
                  'Cyberpunk',
                  intro_ost='Mad Machine',
                  favorite_ost='Victory',
                  popular_ost= 'Hurricane')



# unpacking arguments 
arg = [355,357]

print(list(range(*arg)))

#keyword unpacking

dic_cyberpunk  = {'anime':'BubbleGum Crisis',
                  'year':'2077',
                  'ostOpen':'Victory',
                  'staring':'Knight sabers' }

def cyberpunk(**keywords):
    for kw in keywords:
        print(kw,keywords[kw])


cyberpunk(**dic_cyberpunk)




