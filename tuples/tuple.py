 
# number and vales separated by commas 

tuple_cpu_arch = 'AMD','INTEL','x64',86

print(tuple_cpu_arch)
#output ('AMD', 'INTEL', 'x64', 86)

#first item in tuple 
tuple_cpu_arch[0]

#adding tuples 
tuple_media_name = 'Blade Runner','Cyberpunk 2077','Conquest of paradise'

tuple_media_type = 'movie','game','song'

tuple_media_total = tuple_media_name,tuple_media_type

print(tuple_media_total)


#tuples are immutable
#tuple_cpu_arch[0] = 9
# results in error 
#TypeError: 'tuple' object does not support item assignment








