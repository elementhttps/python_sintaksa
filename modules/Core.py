# core file and entry point for simple app 

# improt modules 
# mod 1 increment by 1 
# mod 2 prints 
#import mod_name_one
#import mod_name_two
# make importing less complicated 
#import mod_name_two as printv2
#from mod_name_one import power_fun as powv2
# import one liner shortcut

from mod_name_two import mod_two_fun as p
from mod_name_one import power_fun as pp


# importing all 
# from mod_name_one import *


# import single function
# from mod_name_one import module_one_fun



# overcomplicated way of printing incrmenting int by one 
#mod_name_two.mod_two_fun((mod_name_one.module_one_fun(10)))


# get module name 
#print(mod_name_one.__name__)

# less complicated way 
#printv2.mod_two_fun(powv2(2))

#one liner
p(pp(2))





