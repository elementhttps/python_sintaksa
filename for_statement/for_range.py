# for and range() function 

# generate 10 values from 0 to 9
for ints in range(10):
    print(ints)


print("-------------------------")
# generate values in between 5 and 10 excluding 10 (5,6,7,8,9)
for ints in range(5,10):
    print(ints)

print("-------------------------")
# generate values between 0 and 10 with step of 2 (0,2,4,6,8) excluding 10 
for ints in range(0,10,2):
    print(ints)

print("-------------------------")
# same but with negative numbers
for ints in range(0,-10,-2):
    print(ints)

# iterating over list 
# create list of FPS keys
list_char_fps_keys = [ 'w','s','a','d']

#iterate over list
for chars in range(len(list_char_fps_keys)):
    print(chars, list_char_fps_keys[chars])




